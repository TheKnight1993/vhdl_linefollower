library IEEE;
use IEEE.std_logic_1164.all;

entity robot is
	port (  clk             : in    std_logic;
		reset           : in    std_logic;

		sensor_l_in     : in    std_logic;
		sensor_m_in     : in    std_logic;
		sensor_r_in     : in    std_logic;

		motor_l_pwm     : out   std_logic;
		motor_r_pwm     : out   std_logic
	);
end entity robot;

architecture big_bot of robot is

	component inputbuffer is
		port (
			clk   : in std_logic;
			sensor_l_in	: in	std_logic;
			sensor_m_in	: in	std_logic;
			sensor_r_in	: in	std_logic;
			sensor_l_out	: out	std_logic;
			sensor_m_out	: out	std_logic;
			sensor_r_out	: out	std_logic
		);
	end component;
	component controller is
		port (
			clk   : in std_logic;
			reset : in std_logic;
			sensor_l		: in	std_logic;
			sensor_m		: in	std_logic;
			sensor_r		: in	std_logic;
			count_in		: in	std_logic_vector (19 downto 0);  -- Please enter upper bound
			count_reset		: out	std_logic;
			motor_l_reset		: out	std_logic := '1';
			motor_l_direction	: out	std_logic;
			motor_r_reset		: out	std_logic := '1';
			motor_r_direction	: out	std_logic
		);
	end component;
	component timebase is
		port (
			clk   : in std_logic;
			reset : in std_logic;
			count_out : out std_logic_vector (19 downto 0)
		);
	end component;
	component motorcontrol is
		port (
			clk   : in std_logic;
			reset : in std_logic;
			direction	: in	std_logic;
			count_in	: in	std_logic_vector (19 downto 0);  -- Please enter upper bound
			pwm		: out	std_logic
		);
	end component;

	signal count_temp : std_logic_vector (19 downto 0);
	signal count_reset, motor_r_reset, motor_l_reset : std_logic;
	signal sensor_l_temp, sensor_r_temp, sensor_m_temp : std_logic;
	signal motor_r_direction_temp, motor_l_direction_temp : std_logic;
	signal motor_r_reset_temp, motor_l_reset_temp : std_logic;

begin

	L1: timebase port map (
		clk => clk, reset => count_reset, 
		count_out => count_temp
	);

	L2: inputbuffer port map (
		clk => clk, 
		sensor_r_in => sensor_r_in, sensor_l_in => sensor_l_in, sensor_m_in => sensor_m_in,
		sensor_l_out => sensor_l_temp, sensor_r_out => sensor_r_temp, sensor_m_out => sensor_m_temp
	);

	L3_1: motorcontrol port map ( --motor R
		clk => clk, reset => motor_r_reset_temp,
		count_in => count_temp,
		direction => motor_r_direction_temp,
		pwm => motor_r_pwm
	);

	L3_2: motorcontrol port map ( --motor L
		clk => clk, reset => motor_l_reset_temp,
		count_in => count_temp,
		direction => motor_l_direction_temp,
		pwm => motor_l_pwm
	);

	L4: controller port map (
		clk => clk, reset => reset,
		sensor_l => sensor_l_temp, sensor_m => sensor_m_temp, sensor_r => sensor_r_temp,
		count_in => count_temp, count_reset => count_reset,
		motor_l_reset => motor_l_reset_temp, motor_r_reset => motor_r_reset_temp,
		motor_l_direction => motor_l_direction_temp, motor_r_direction => motor_r_direction_temp
	);


end architecture;
