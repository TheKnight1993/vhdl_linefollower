library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity inputbuffer_tb is
end entity;

architecture tb of inputbuffer_tb is
    component inputbuffer is
        port (
            clk   : in std_logic;
            sensor_l_in	: in	std_logic;
		    sensor_m_in	: in	std_logic;
		    sensor_r_in	: in	std_logic;
    		sensor_l_out	: out	std_logic;
	    	sensor_m_out	: out	std_logic;
		    sensor_r_out	: out	std_logic
        );
    end component;

    signal sensor_l, sensor_m, sensor_r, sensor_l_out, sensor_r_out, sensor_m_out : std_logic := '0';
    signal clk : std_logic;
    
    
begin

    clk <= '1' after 0 ns, 
           '0' after 10 ns when clk /= '0' else '1' after 10 ns;
    sensor_l <= '1' after 69 ns, '0' after 75 ns, '1' after 94 ns, '0' after 111 ns;
    sensor_m <= '1' after 42 ns, '0' after 60 ns, '1' after 86 ns, '0' after 120 ns;

    tb1: inputbuffer port map (
        clk => clk,
        sensor_l_in => sensor_l, sensor_m_in => sensor_m, sensor_r_in => sensor_r,
        sensor_l_out => sensor_l_out, sensor_m_out => sensor_m_out, sensor_r_out => sensor_r_out
    );

end architecture;