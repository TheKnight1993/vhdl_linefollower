library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;


entity registry_3_bits is --3 bit registry with 3 d-flipflops
	port (
		clk   : in std_logic;
		in_1  : in std_logic;
		in_2  : in std_logic;
		in_3  : in std_logic;
		out_1 : out std_logic;
		out_2 : out std_logic;
		out_3 : out std_logic
	);
end entity;

architecture behavioural of registry_3_bits is

begin

	process (clk) --process statement for the 3 d-flipflops
	begin
		if rising_edge(clk) then
			out_1 <= in_1;
			out_2 <= in_2;
			out_3 <= in_3;
		end if;
	end process;

end architecture;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;


entity inputbuffer is --synchronise those stupid sensors
	port (	clk		: in	std_logic;
		sensor_l_in	: in	std_logic;
		sensor_m_in	: in	std_logic;
		sensor_r_in	: in	std_logic;
		sensor_l_out	: out	std_logic;
		sensor_m_out	: out	std_logic;
		sensor_r_out	: out	std_logic
	);
end entity inputbuffer;

architecture rtl of inputbuffer is
	component registry_3_bits is --call upon the registry
		port (
			clk   : in std_logic;
			in_1  : in std_logic;
			in_2  : in std_logic;
			in_3  : in std_logic;
			out_1 : out std_logic;
			out_2 : out std_logic;
			out_3 : out std_logic
		);
	end component;
	signal sensor_l_temp, sensor_m_temp, sensor_r_temp : std_logic; 
	
begin

	l1: registry_3_bits port map ( --first stage
		clk => clk,
		in_1 => sensor_l_in, in_2 => sensor_m_in, in_3 => sensor_r_in,
		out_1 => sensor_l_temp, out_2 => sensor_m_temp, out_3 => sensor_r_temp
	);
	l2: registry_3_bits port map ( --second stage
		clk => clk,
		in_1 => sensor_l_temp, in_2 => sensor_m_temp, in_3 => sensor_r_temp,
		out_1 => sensor_l_out, out_2 => sensor_m_out, out_3 => sensor_r_out
	);

end architecture;
