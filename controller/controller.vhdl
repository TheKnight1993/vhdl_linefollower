library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
-- Please add necessary libraries:


entity controller is
	port (	clk			: in	std_logic;
		reset			: in	std_logic;

		sensor_l		: in	std_logic;
		sensor_m		: in	std_logic;
		sensor_r		: in	std_logic;

		count_in		: in	std_logic_vector (19 downto 0);  -- Please enter upper bound
		count_reset		: out	std_logic;

		motor_l_reset		: out	std_logic := '1';
		motor_l_direction	: out	std_logic;

		motor_r_reset		: out	std_logic := '1';
		motor_r_direction	: out	std_logic
	);
end entity controller;

architecture rtl of controller is
	type control_state is (
		ready, gentle_left, gentle_right, sharp_left, sharp_right, forward, hold
	);
	signal cur_state, new_state : control_state;
	
begin

	process (clk) --fsm process
	begin
		if rising_edge(clk) then
			if reset = '1' then
				new_state <= ready;	
				count_reset <= '1';
				motor_l_reset <= '1';
				motor_r_reset <= '1';
			end if;
			if reset = '0' then
			case cur_state is
				when ready =>
					if unsigned(count_in) = 0 then
						count_reset <= '0';
						motor_l_reset <= '0';
						motor_r_reset <= '0';
						if (sensor_l = '0' and sensor_m = '0' and sensor_r = '1') then
							motor_l_direction <= '0';
							motor_r_direction <= '1';
							new_state <= gentle_left;
						elsif (sensor_l = '0' and sensor_m = '1' and sensor_r = '1') then
							motor_l_direction <= '1'; 
							motor_r_direction <= '1';
							new_state <= sharp_left;
						elsif (sensor_l = '1' and sensor_m = '0' and sensor_r = '0') then
							motor_l_direction <= '1';
							motor_r_direction <= '0';
							new_state <= gentle_right;
						elsif (sensor_l = '1' and sensor_m = '1' and sensor_r = '0') then
							motor_l_direction <= '1';
							motor_r_direction <= '1';
							new_state <= sharp_right;
						else
							motor_l_direction <= '1';
							motor_r_direction <= '1';
							new_state <= forward;
						end if;
					end if;
				--following cases allows the controller to reset the motorcontrol
				--thus making it so the motorcontrol is not outputting a pwm signal
				-- TODO remove the motor reset statements
				-- TODO check if possible to implement count in motorcontrol or keep controller in control
				when forward =>
					if unsigned(count_in) = 50000 then
						motor_r_direction <= '0';
						-- motor_l_reset <= '1';
					end if;
					if unsigned(count_in) = 100000 then
						motor_l_direction <= '0';
						-- motor_r_reset <= '1';
						new_state <= hold;
					end if;
				when gentle_left =>
					if unsigned(count_in) = 50000 then
						motor_r_direction <= '0';
						-- motor_r_reset <= '1';
						-- motor_l_reset <= '1';
						new_state <= hold;
					end if;
				when gentle_right =>
					if unsigned(count_in) = 50000 then
						motor_l_direction <= '0';
						-- motor_l_reset <= '1';
						-- motor_r_reset <= '1';
						new_state <= hold;
					end if;
				when sharp_left =>
					if unsigned(count_in) = 50000 then
						motor_r_direction <= '0';
						-- motor_r_reset <= '1';
						motor_l_direction <= '0';
						-- motor_l_reset <= '1';
						new_state <= hold;
					end if;
				when sharp_right =>
					if unsigned(count_in) = 100000 then
						motor_r_direction <= '0';
						-- motor_r_reset <= '1';
						motor_l_direction <= '0';
						-- motor_l_reset <= '1';
						new_state <= hold;
					end if;
					--final state, waiting for the next pwm cycle
				when hold => 
					if unsigned(count_in) = x"F423D" then --make sure the count is stopped at 1 million, including delay times
						new_state <= ready;
						count_reset <= '1';
					end if;
			end case;
			end if;
		end if;
	end process;

	process (clk, new_state) --registry process
	begin
		if rising_edge(clk) then
			if reset = '1' then
				cur_state <= ready;	
				count_reset <= '1';
				motor_l_reset <= '1';
				motor_r_reset <= '1';
			else
				cur_state <= new_state;
			end if;
		end if;
	end process;

end architecture;