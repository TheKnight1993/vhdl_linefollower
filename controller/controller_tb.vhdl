library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity controller_tb is
end entity;

architecture testbench of controller_tb is
    component motorcontrol is
        port (
            clk   : in std_logic;
            reset : in std_logic;
            count_in	: in	std_logic_vector (19 downto 0);  -- Please enter upper bound
            pwm		: out	std_logic
        );
    end component;
    component controller is
        port (
            clk   : in std_logic;
            reset : in std_logic;
            sensor_l		: in	std_logic;
		    sensor_m		: in	std_logic;
		    sensor_r		: in	std_logic;
		    count_in		: in	std_logic_vector (19 downto 0);  -- Please enter upper bound
		    count_reset		: out	std_logic := '1';
		    motor_l_reset		: out	std_logic := '1';
		    motor_l_direction	: out	std_logic;
		    motor_r_reset		: out	std_logic := '1';
		    motor_r_direction	: out	std_logic
        );
    end component;
    component timebase is
        port (
            clk   : in std_logic;
            reset : in std_logic;
            count_out : out std_logic_vector (19 downto 0)
        );
    end component;
    component inputbuffer is
        port (
            clk   : in std_logic;
            sensor_l_in	: in	std_logic := '0';
		    sensor_m_in	: in	std_logic := '0';
		    sensor_r_in	: in	std_logic := '0';
    		sensor_l_out	: out	std_logic;
	    	sensor_m_out	: out	std_logic;
		    sensor_r_out	: out	std_logic
        );
    end component;
    signal count_reset, clk, reset: std_logic;
    signal count_temp : std_logic_vector (19 downto 0);
    signal motor_l_direction, motor_r_direction, motor_l_reset, motor_r_reset: std_logic;
    signal sensor_l, sensor_m, sensor_r, sensor_l_out, sensor_r_out, sensor_m_out : std_logic := '0';
    signal pwm_r, pwm_l : std_logic;
    
begin
    reset <= '1' after 0 ns,
             '0' after 51 ns;
    clk <= '1' after 0 ns, 
           '0' after 10 ns when clk /= '0' else '1' after 10 ns;
    --first 20ms of r, m, l all 0 -> 1st option
    sensor_r <= '1' after 19999980 ns, '0' after 20000020 ns, --second option
    '1' after 59999980 ns, '0' after 60000020 ns, --4th option
    '1' after 99999980 ns, '0' after 100000020 ns, --6th option
    '1' after 139999980 ns, '0' after 140000020 ns; --8th option

    sensor_m <= '1' after 40000000 ns, '0' after 60000020 ns, --3th and 4th
    '1' after 120000000 ns, '0' after 140000020 ns;

    sensor_l <= '1' after 79999980 ns, '0' after 140000020 ns;
    L1: timebase port map (
        clk => clk, reset => count_reset, count_out => count_temp
    );
    L2: controller port map (
        clk => clk, reset => reset, sensor_l => sensor_l_out, sensor_r => sensor_r_out, sensor_m => sensor_m_out,
        count_reset => count_reset, count_in => count_temp,
        motor_l_direction => motor_l_direction, motor_r_direction => motor_r_direction,
        motor_l_reset => motor_l_reset, motor_r_reset => motor_r_reset
    );
    L3: inputbuffer port map (
        clk => clk,
        sensor_l_in => sensor_l, sensor_m_in => sensor_m, sensor_r_in => sensor_r,
        sensor_l_out => sensor_l_out, sensor_m_out => sensor_m_out, sensor_r_out => sensor_r_out
    );

    L4ML: entity work.motorcontrol(rtl) port map (
        clk => clk, reset => motor_l_reset, count_in => count_temp, pwm => pwm_l, direction => motor_l_direction
    );

    L5MR: entity work.motorcontrol(rtl) port map (
        clk => clk, reset => motor_r_reset, count_in => count_temp, pwm => pwm_r, direction => motor_r_direction
    );
end architecture;