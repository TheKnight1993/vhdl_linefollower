library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;


entity motorcontrol is
	port (	clk		: in	std_logic;
		reset		: in	std_logic;
		direction	: in	std_logic;
		count_in	: in	std_logic_vector (19 downto 0);  -- Please enter upper bound
		pwm		: out	std_logic := '1'
	);
end entity motorcontrol;

--test architecture of the motorcontrol
--architecture arch of motorcontrol is
--	type motorcontrol_state is (
--		motor_left,
--		motor_right
--	);
--	signal count : unsigned (19 downto 0);
--	signal cur_state, new_state : motorcontrol_state; --2 states, 0 for left, 1 for right, according to direction
--
--begin
--	count <= unsigned(count_in);
--
--	process (clk, cur_state) --fsm implementation, moore machine as output is dependent on the current state
--	begin
--		if (count = 0) then --check for when count equals to 0 for clock reduction
--			case cur_state is
--				when motor_left =>
--					pwm <= '1' after 0 ns, 
--					'0' after 1 ms; --set output signal
--
--					if (direction = '1') then --if case to choose new state
--						new_state <= motor_right;
--					else
--						new_state <= motor_left;
--					end if;
--				
--				when motor_right =>
--					pwm <= '1' after 0 ns,
--					'0' after 2 ms; -- set output signal
--
--					if (direction = '0') then --if case to choose new state
--						new_state <= motor_left;
--					else
--						new_state <= motor_right;
--					end if;
--			end case;
--		end if;
--	end process;
--	
--	process (clk) --state registry
--	begin
--		if (clk'event and clk = '1') then
--			if (reset = '1') then
--				cur_state <= motor_left;
--			else
--				cur_state <= new_state;
--			end if;
--		end if;
--	end process;
--	
--
--end architecture;

-- new architecture for synthesis and use in the fpga board
architecture rtl of motorcontrol is
	type drive_state is (
		initisalise, run, done
		);
	signal cur_state, new_state : drive_state;
	
begin

	process (clk) 
	begin
		if rising_edge(clk) then
			--reset the motorcontrol instance
			if reset = '1' then
				new_state <= initisalise;
				pwm <= '0';
			else
				case cur_state is
					when initisalise =>
						if direction = '1' then
							new_state <= run;
							pwm <= '1';
						end if;
					when run =>
						if direction = '0' then
							new_state <= done;
							pwm <= '0';
						end if;
					when done => 
						if unsigned(count_in) = x"F423D" then --make sure the count is stopped at 1 million, including delay times
							new_state <= initisalise;
						end if;
		
				end case;
			end if;
		end if;
	end process;

	process (clk)
	begin
		if rising_edge(clk) then
			cur_state <= new_state;
		end if;
	end process;

end architecture;

-- new architecture for synthesis and use in the fpga board
architecture rtl of motorcontrol is
	type drive_state is (
		initisalise, run, done
		);
	signal cur_state, new_state : drive_state;
	
begin

	process (clk) 
	begin
		if rising_edge(clk) then
			--reset the motorcontrol instance
			case cur_state is
				when initisalise =>
					if direction = '1' then
						new_state <= run;
						pwm <= '1';
					end if;
				when run =>
					if direction = '0' then
						new_state <= done;
						pwm <= '0';
					end if;
				when done =>  -- TODO check if this count is correct for the motorcontrol
					if unsigned(count_in) = x"F423D" then --make sure the count is stopped at 1 million, including delay times
						new_state <= initisalise;
					end if;
	
				end case;
		end if;
	end process;

	process (clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				cur_state <= initisalise;
				pwm <= '0';
			else
				cur_state <= new_state;
			end if;
		end if;
	end process;

end architecture;