library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity motorcontrol_tb is
end entity motorcontrol_tb;

architecture rtl of motorcontrol_tb is
    component motorcontrol is
        port (
            clk   : in std_logic;
            reset : in std_logic;
            direction : in std_logic;
            count_in : in std_logic_vector(19 downto 0);
            pwm : out std_logic
        );
    end component;
    component timebase is
        port (
            clk   : in std_logic;
            reset : in std_logic;
            count_out : out std_logic_vector (19 downto 0)
        );
    end component;
    signal direction, reset, clk, pwm: std_logic;
    signal count : std_logic_vector (19 downto 0);
    
begin

    reset <=    '1' after 0 ns,
                '0' after 11 ns;
    clk <=    '1' after 0 ns, 
                '0' after 10 ns when clk /= '0' else '1' after 10 ns;
    direction<= '0' after 0 ns,
                '1' after 20000000 ns;

    T1 : motorcontrol port map (
        clk => clk, reset => reset, direction => direction, count_in => count, pwm => pwm
    );
    T2 : timebase port map (
        clk => clk, reset => reset, count_out => count
    );

end architecture;