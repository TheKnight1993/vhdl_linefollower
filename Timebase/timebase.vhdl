library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity timebase is
	port (	clk		: in	std_logic;
		reset		: in	std_logic;
		count_out	: out	std_logic_vector (19 downto 0)  -- Please enter upper bound
	);
end entity timebase;

architecture behavioral of timebase is

	signal count, new_count : unsigned(19 downto 0);
begin

	process (clk)
	begin
		if(clk'event and clk = '1') then
			count <= new_count;
		end if;
	end process;

	process(count)
	begin
		if (reset = '1') then
			count <= (others => '0');
		else new_count <= count + 1;
		end if;
	end process;

	count_out <= std_logic_vector (count);

end architecture behavioral;
