library IEEE;
use IEEE.std_logic_1164.all;

entity timebase_tb is
end entity;

architecture testbench of timebase_tb is
    component timebase is
        port (
            clk   : in std_logic;
            reset : in std_logic;
            count_out : out std_logic_vector(19 downto 0)
        );
    end component;

    signal clock : std_logic;
    signal reset : std_logic;
    signal count_out : std_logic_vector(19 downto 0 );
    
begin
    reset <=    '1' after 0 ns,
                '0' after 11 ns,
                '1' after 20000020 ns,
                '0' after 20000031 ns;
    clock <=    '1' after 0 ns, 
                '0' after 10 ns when clock /= '0' else '1' after 10 ns;

    
    T1: timebase port map (
        clk => clock, reset => reset, count_out => count_out
    );  

end architecture testbench;